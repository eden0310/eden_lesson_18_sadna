#include <iostream>
#include "Windows.h"
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include "Helper.h"
#include <direct.h>
#define GetCurrentDir _getcwd

using namespace std;

string GetCurrentWorkingDir(void);
int fileExists(const char * file);

int main()
{
	vector<string> words;
	string str;
	while (true)
	{
		cout << ">>";
		getline(cin, str);
		words = Helper::get_words(str);
		
		
		if (words[0] == "pwd") // Prints the current directorys
		{
			cout << GetCurrentWorkingDir().c_str() << endl;
		}
		else if (words[0] == "cd") // ** Changes the current directory ** //
		{
			if (words.size() != 2){
				cout << "Wrong parameter number\nExpected : 2 Actual : " << words.size() << endl;
			}
			else {
				string dir = words.at(1);
				if (!SetCurrentDirectoryA(dir.c_str())){
					cout << "Failed to change dir error: " << GetLastError() << endl;
				}
			}
		} 
		else if (words.at(0) == "create") // ** Creates and overwrites files ** //
		{
				if (words.size() != 2) {
					cout << "Wrong parameter number\nExpected : 2 Actual : " << words.size() << endl;
				}
				else {
					HANDLE hfile = CreateFile(
						words.at(1).c_str(),
						GENERIC_READ,
						0,
						NULL,
						CREATE_ALWAYS,
						FILE_ATTRIBUTE_NORMAL,
						NULL);
					CloseHandle(hfile);
				}
		}
		else if(words.at(0) == "ls") // ** Lists files in current working directory **//
		{
			HANDLE hFind;
			WIN32_FIND_DATA data;

			string match = GetCurrentWorkingDir() + "\\*";
			hFind = FindFirstFile(match.c_str(), &data);
			if (hFind != INVALID_HANDLE_VALUE) {
				do {
					//if (data.cFileName != "." && data.cFileName != "..") {
					cout << data.cFileName << endl;
				} while (FindNextFile(hFind, &data));
				FindClose(hFind);
			}
		}
		else if (words.at(0) == "secret") // ** Prints the secret number ** //
		{
			HMODULE dll = LoadLibrary("SECRET.dll");
			if (dll == NULL) {
				cout << "Failed to load dll\nerror: " << GetLastError() << endl;
			}
			else {
				int address = (int)GetProcAddress(dll, "TheAnswerToLifeTheUniverseAndEverything");
				cout << "TheAnswerToLifeTheUniverseAndEverything: " << ((int(*)(void))address)() << endl;
			}
		}
		else if (fileExists(string(GetCurrentWorkingDir() + "\\" + words.at(0)).c_str())) // Checks if theres a file with that name to run
		{
			system(string(GetCurrentWorkingDir() + "\\" + words.at(0)).c_str());
			cout << endl;
		}
		else if (fileExists(string(GetCurrentWorkingDir() + "\\" + words.at(0) + ".exe").c_str())) {
			system(string(GetCurrentWorkingDir() + "\\" + words.at(0) + ".exe").c_str());
			cout << endl;
		}
	}

	return 0;
}


string GetCurrentWorkingDir(void)
{
	char buff[FILENAME_MAX];
	GetCurrentDir(buff, FILENAME_MAX);
	string current_working_dir(buff);
	return current_working_dir;
}

//checks if a file in a path exists
int fileExists(const char * file)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE handle = FindFirstFile(file, &FindFileData);
	int found = handle != INVALID_HANDLE_VALUE;
	if (found) {
		//FindClose(&handle); this will crash
		FindClose(handle);
	}
	return found;
}